package com.test.natixis.models;

public class Departement {
    private String nom;
    private int code;
    private int codeRegion;

    public Departement(String nom, int code, int codeRegion) {
        this.nom = nom;
        this.code = code;
        this.codeRegion = codeRegion;
    }

    public String getNom() { return nom; }
    public int getCode() { return code; }
    public int getCodeRegion() { return codeRegion; }

    public void setNom(String nom) { this.nom = nom; }
    public void setCode(int code) { this.code = code; }
    public void setCodeRegion(int codeRegion) { this.codeRegion = codeRegion; }
}
