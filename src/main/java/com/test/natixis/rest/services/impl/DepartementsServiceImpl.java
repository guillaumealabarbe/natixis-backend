package com.test.natixis.rest.services.impl;
import com.test.natixis.models.Departement;
import com.test.natixis.rest.services.DepartementsService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DepartementsServiceImpl implements DepartementsService {
    private static List<Departement> _departements = new ArrayList<Departement>() {{
        add(new Departement("Calvados", 14, 28));
        add(new Departement("Eure", 27, 28));
        add(new Departement("Manche", 50, 28));
        add(new Departement("Orne", 61, 28));
        add(new Departement("Seine-Maritime", 76, 28));
    }};

    @Override
    public List<Departement> getDepartements() {
        return this._departements;
    }

    @Override
    public Departement getDepartement(String nom) {
        return this._departements.stream()
                .filter(departement -> nom.equals(departement.getNom()))
                .findAny()
                .orElse(null);
    }
}
