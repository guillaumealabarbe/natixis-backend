package com.test.natixis.rest.services;
import com.test.natixis.models.Departement;
import java.util.List;

public interface DepartementsService {
    List<Departement> getDepartements();

    Departement getDepartement(String nom);
}
