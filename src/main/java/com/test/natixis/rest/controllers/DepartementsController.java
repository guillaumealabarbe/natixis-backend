package com.test.natixis.rest.controllers;
import com.test.natixis.models.Departement;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.List;

@CrossOrigin(maxAge = 3600)
public interface DepartementsController {
    @GetMapping(value = "departements")
    ResponseEntity<List<Departement>> departements(@RequestParam(required = false, defaultValue = "") String nom);
}
