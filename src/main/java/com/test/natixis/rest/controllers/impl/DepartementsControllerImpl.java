package com.test.natixis.rest.controllers.impl;
import com.test.natixis.models.Departement;
import com.test.natixis.rest.controllers.DepartementsController;
import com.test.natixis.rest.services.DepartementsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController("departements")
public class DepartementsControllerImpl implements DepartementsController {
    private DepartementsService departementsService;

    @Autowired
    public DepartementsControllerImpl(DepartementsService departementsService) {
        this.departementsService = departementsService;
    }

    public ResponseEntity<List<Departement>> departements(String nom) {
        if (nom.equals("")) {
            return ResponseEntity.ok(departementsService.getDepartements());
        } else {
            return ResponseEntity.ok(new ArrayList<Departement>() {{
                add(departementsService.getDepartement(nom));
            }});
        }
    }
}
