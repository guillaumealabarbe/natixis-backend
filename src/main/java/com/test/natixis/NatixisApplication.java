package com.test.natixis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NatixisApplication {

	public static void main(String[] args) {
		SpringApplication.run(NatixisApplication.class, args);
	}

}
